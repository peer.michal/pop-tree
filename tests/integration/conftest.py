import functools
import json
import pathlib
import subprocess
import sys
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="function")
def hub():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="tree")
    hub.pop.sub.add("tests.integration.mod", omit_class=False)
    with mock.patch("sys.exit"):
        yield hub


def cli(subcommand: str, *args, **kwargs):
    cmd = [sys.executable, subcommand, *args]
    if not any("--output" in c for c in cmd):
        cmd.append("--output=json")

    proc = subprocess.check_output(cmd, **kwargs)
    stdout = proc.decode()

    try:
        return json.loads(stdout)
    except:
        return stdout


@pytest.fixture(scope="function", name="cli_doc")
def get_doc_cli():
    return functools.partial(
        cli, str(pathlib.Path(__file__).parent.parent.parent / "popdoc.py")
    )


@pytest.fixture(scope="function", name="cli_tree")
def get_tree_cli():
    return functools.partial(
        cli, pathlib.Path(__file__).parent.parent.parent / "run.py"
    )
